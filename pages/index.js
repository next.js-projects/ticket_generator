import React, { useRef } from 'react'

import Head from 'next/head'

export default function Home({user}) {

  const inputRef = useRef()

  return (
    <div className="card home">
      <Head>
        <title>Ticket do Github</title>

        <meta property="og:title" content="Ticket do Github" key="Ticket do Github" />
        <meta property="og:site_name" content={`Gerador de Ticket do Github`} key="ogsitename" />
        <meta name="description" content="Gerador de Ticket de usuários do Github" />
        <meta property="og:description" content="Gerador de Ticket de usuários do Github" key="ogdesc" />

        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
      </Head>


      <h1>Crie seu card do Github</h1>
      <h3>e compartilhe com seus amigos</h3>

      <div className="user-login--input">
        <label htmlFor="user-github">Seu usuário do Github</label>
        <input type="text" ref={inputRef} className="user-github" name="user-github" />
        <small>
          <p>Adicione um usuário válido, veja seu login:</p>
          <p><code>https://github.com/[seu usuário]</code></p>
        </small>
      </div>

      <button className="create-btn" onClick={() => {location.href += inputRef.current.value}}>Criar</button>
    </div>
  )
}
