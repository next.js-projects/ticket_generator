import Head from 'next/head'
import { useRouter } from 'next/router'

export default function Users({user}) {

  const { isFallback } = useRouter()

  return (
    <div className="card">      
      {
        isFallback ? 
        (
          <>
            <h1>Seu card está sendo criado...</h1>
            <h3>Logo mais estará de forma estática!</h3>
          </>
        )
        :
        (
          <>
            <Head>
              <title>{user.name}</title>
              <link rel="icon" type="image/png" sizes="96x96" href={user.avatar_url}></link>

              <meta property="og:title" content={user.name} key={user.name} />
              <meta property="og:site_name" content={`Ticket do ${user.name}`} key="ogsitename" />
              <meta name="description" content={user.bio} />
              <meta property="og:description" content={user.bio} key="ogdesc" />
              <meta property="og:image" content={user.avatar_url} key="ogimage" />

              <meta name="viewport" content="width=device-width, initial-scale=1" />
              <meta charSet="utf-8" />
            </Head>

            <h1>{user.name}</h1>
            <img src={user.avatar_url} />
      
            <h3 className="bio">{user.bio}</h3>
      
            <p>Git: <a href={user.html_url} target="_blank">{user.login}</a></p>
            <p>Site: <a href={user.blog} target="_blank">{user.blog}</a></p>
          </>
        )
      }

    </div>
  )
}

export async function getStaticProps({ params }) {

  const response = await fetch(`https://api.github.com/users/${params.id}`)
  const user = await response.json()

  return {
    props: {
      user,
    },
    revalidate: 100,
  }
}

export async function getStaticPaths() {

  const paths = 
  ['giovannipr', 'diego3g']
  .map((user) => ({
    params: { id: user },
  }))

  return { paths, fallback: true /* os que não forem listados, serão gerados apenas quando acessados */ }
}
